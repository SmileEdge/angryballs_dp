package angryballs.outilsvues.Dessinateur;

import angryballs.Visiteur.Visiteur;
import angryballs.modele.Bille.Bille;

import java.awt.*;

public class VisiteurDessinBille implements Visiteur {

    Graphics g;

    public VisiteurDessinBille(Graphics g)
    {
        this.g = g;
    }

    @Override
    public void visite(Bille bille) {


        int width, height;
        int xMin, yMin;

        xMin = (int)Math.round(bille.getPosition().x-bille.getRayon());
        yMin = (int)Math.round(bille.getPosition().y-bille.getRayon());

        width = height = 2*(int)Math.round(bille.getRayon());

        g.setColor(bille.getCouleur());
        g.fillOval( xMin, yMin, width, height);
        g.setColor(Color.CYAN);
        g.drawOval(xMin, yMin, width, height);



    }
}
