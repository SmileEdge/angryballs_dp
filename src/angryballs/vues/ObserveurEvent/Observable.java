package angryballs.vues.ObserveurEvent;

import angryballs.AnimationBilles;

import java.awt.event.ActionListener;

public interface Observable {
    public void addObservateur(Observateur observateur);
    public void notifyObservers();
}
