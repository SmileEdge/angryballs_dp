package angryballs.vues.ObserveurEvent;

public interface Observateur {

    public void actualiser(Observable observable);
}
