package angryballs.vues;

import java.awt.*;
import java.awt.event.MouseListener;
import java.util.Vector;

import angryballs.modele.Bille.Bille;

import angryballs.outilsvues.EcouteurTerminaison;

import angryballs.outilsvues.Outils;
import angryballs.vues.ObserveurEvent.ButtonObservable;

import javax.swing.*;

/**
 * Vue dessinant les billes et contenant les 3 boutons de contr�le (arr�t du programme, lancer les billes, arr�ter les billes) 
 * 
 *  ICI : IL N'Y A RIEN A CHANGER 
 *  
 * 
 * */
public class CadreAngryBalls extends Frame implements VueBillard
{
    private TextField presentation;
    private Billard billard;
    public ButtonObservable lancerBilles, arreterBilles;
    //Panel haut;
    private JPanel centre;

    private EcouteurTerminaison ecouteurTerminaison;

    public CadreAngryBalls(String titre, String message, Vector<Bille> billes) throws HeadlessException
    {
        super(titre);
        Outils.place(this, 0.33, 0.33, 0.5, 0.5);
        this.ecouteurTerminaison = new EcouteurTerminaison(this);
        //this.haut = new Panel(); this.haut.setBackground(Color.LIGHT_GRAY);
        //this.add(this.haut,BorderLayout.NORTH);

        this.centre = new JPanel();
        //this.centre.setDoubleBuffered(true);
        this.setIgnoreRepaint(true);
        this.add(this.centre,BorderLayout.CENTER);
        JPanel bas = new JPanel();
        bas.setBackground(Color.LIGHT_GRAY);
        this.add(bas,BorderLayout.SOUTH);

        //this.presentation = new TextField(message, 100); this.presentation.setEditable(false);
        //this.haut.add(this.pr�sentation);

        this.billard = new Billard(billes);
        this.add(this.billard);

        this.lancerBilles = new ButtonObservable("lancer les billes"); bas.add(this.lancerBilles);
        this.arreterBilles = new ButtonObservable("arr�ter les billes"); bas.add(this.arreterBilles);
    }

    public double largeurBillard()
    {
    return this.billard.getWidth();
    }

    public double hauteurBillard()
    {
    return this.billard.getHeight();
    }

    @Override
    public void miseAJour()
    {
        this.billard.myRepaint();
    }

    /* (non-Javadoc)
     * @see exodecorateur.angryballs.vues.VueBillard#montrer()
     */
    @Override
    public void montrer()
    {
    this.setVisible(true);
    }

    public Billard getBillard() {
        return billard;
    }

    @Override
    public void addMouseListener(MouseListener mouseListener)
    {
        this.billard.addMouseListener(mouseListener);
    }
}