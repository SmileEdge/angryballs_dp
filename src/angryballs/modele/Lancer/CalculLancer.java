package angryballs.modele.Lancer;

import angryballs.AnimationBilles;
import mesmaths.geometrie.base.Vecteur;

public class CalculLancer extends Thread {
    private Thread thread = null;
    private long startTime, endTime;
    public int elapsedTime;
    private AnimationBilles animationBilles;

    public CalculLancer(AnimationBilles animationBilles) {
        this.animationBilles = animationBilles;
        elapsedTime = 0;
    }

    @Override
    public void run() {
        try {
            startTime = System.currentTimeMillis();
            double masse = animationBilles.billeCourante.masse() / 1000;
            Vecteur accSouris = animationBilles.souris.getAcceleration(),
                    accBille = animationBilles.billeCourante.getAccélération(),
                    newAcc = new Vecteur(-(accSouris.x + accBille.x) / masse, -(accSouris.y + accBille.y) / masse);
            while (true) {
                animationBilles.billeCourante.getAccélération().set(newAcc);
                endTime = System.currentTimeMillis();
                elapsedTime += (int) (endTime - startTime);
                Thread.sleep(10);
            }
        }
        catch (InterruptedException e)
        {
        }
    }

    public void lancer()
    {
        if (this.thread == null)
        {
            this.thread = new Thread(this);
            thread.start();
        }
    }

    public void arreter()
    {
        if (thread != null)
        {
            this.thread.interrupt();
            this.thread = null;
        }
    }
}
