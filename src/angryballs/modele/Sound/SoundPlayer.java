package angryballs.modele.Sound;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.SourceDataLine;

public class SoundPlayer implements Runnable{
    private FileInputStream f;
    private File pere, file, here;

    private Thread thread;
    private HashMap<String,File> files;

    public SoundPlayer(String nameFile)
    {
        pere = new File("");
        here = new File(pere.getAbsoluteFile(), "sounds");
        file = new File(here.getAbsoluteFile(), nameFile);
    }

    public void playSound()
    {
        AudioFormat format;
        SourceDataLine ligne;
        int tailleFrame, m, tailleTampon, reste;
        byte[] tampon;
        long l, q, r, p;
        try{
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
            format = audioInputStream.getFormat();

            ligne = AudioSystem.getSourceDataLine(format);
            ligne.open(format);

            tailleFrame = format.getFrameSize();
            m = (int)(0.01*format.getFrameRate());
            tailleTampon = m*tailleFrame;
            tampon = new byte[tailleTampon];

            ligne.start();

            l=audioInputStream.getFrameLength();

            q = l/m;
            r = l%m;
            reste = (int)(r*tailleFrame);
            for (p = 0; p < q; ++p)
            {
                audioInputStream.read(tampon);
                ligne.write(tampon, 0 , tampon.length);
            }
            audioInputStream.read(tampon, 0, reste);
            ligne.write(tampon, 0 , reste);
        }
        catch(Exception e){

            System.out.println(e.getMessage());
        }
    }


    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
       this.playSound();
    }

    public void lancerSon()
    {
        if (this.thread == null)
        {
            this.thread = new Thread(this);
            thread.start();
        }
    }

    public void arreterSon()
    {
        if (thread != null)
        {
            this.thread.interrupt();
            this.thread = null;
        }
    }
}

