package angryballs.modele.Souris;

import angryballs.modele.Comportements.BillePilotee.BillePilotée;
import mesmaths.geometrie.base.Vecteur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class Souris implements MouseMotionListener {
    private Vecteur vitesse;
    private Vecteur oldPosSouris, newPosSouris;
    private Vecteur acceleration;

    public Vecteur getVitesse() {
        return vitesse;
    }

    public Vecteur getOldPosSouris() {
        return oldPosSouris;
    }

    public Vecteur getNewPosSouris() {
        return newPosSouris;
    }

    public Vecteur getAcceleration() {
        return acceleration;
    }

    public Souris() {
        this.oldPosSouris = new Vecteur(-1, -1);
        this.newPosSouris = new Vecteur();
        this.acceleration = new Vecteur();
        this.vitesse = new Vecteur();
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        this.calcAcceleration(mouseEvent);
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        this.calcAcceleration(mouseEvent);
    }

    public void calcAcceleration(MouseEvent mouseEvent)
    {
        double deltaX, deltaY;

        if (oldPosSouris.x == -1) {
            oldPosSouris.x = newPosSouris.x;
            oldPosSouris.y = newPosSouris.y;
        }

        newPosSouris.x = mouseEvent.getX();
        newPosSouris.y = mouseEvent.getY();
        deltaX = (oldPosSouris.x - newPosSouris.x);
        acceleration.x = deltaX;
        vitesse.x = deltaX;
        deltaY = (oldPosSouris.y - newPosSouris.y);
        acceleration.y = deltaY;
        vitesse.y = deltaY;

        oldPosSouris.x = newPosSouris.x;
        oldPosSouris.y = newPosSouris.y;
    }
}
