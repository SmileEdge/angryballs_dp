package angryballs.modele.Bille;

import angryballs.AnimationBilles;
import angryballs.Visiteur.Visitable;
import angryballs.Visiteur.Visiteur;
import angryballs.modele.Bille.Bille;
import angryballs.modele.Souris.Souris;
import mesmaths.cinematique.Cinematique;
import mesmaths.geometrie.base.Vecteur;

import java.awt.*;
import java.util.Vector;

public  class BilleAvecComportement extends Bille  {


    private Vecteur position;   // centre de la bille
    private  double rayon;            // rayon > 0
    private  Vecteur vitesse;
    private  Vecteur accélération;
    private int clef;                // identifiant unique de cette bille


    private Color couleur;

    private static int prochaineClef = 0;

    public static double ro = 1;        // masse volumique


    public BilleAvecComportement(Vecteur centre, double rayon, Vecteur vitesse,
                    Vecteur accélération, Color couleur)
    {
        this.position = centre;
        this.rayon = rayon;
        this.vitesse = vitesse;
        this.accélération = accélération;
        this.couleur = couleur;
        this.clef = BilleAvecComportement.prochaineClef ++;
    }

    public BilleAvecComportement(Vecteur position, double rayon, Vecteur vitesse, Color couleur) {
        this(position, rayon, vitesse, new Vecteur(), couleur);
    }
    public Vecteur getPosition() {
        return position;
    }

    protected void setPosition(Vecteur position)
    {
        this.position = position;
    }

    public double getRayon() {
        return rayon;
    }

    protected void setRayon(double rayon)
    {
        this.rayon = rayon;
    }

    public Vecteur getVitesse() {
        return vitesse;
    }

    public Vecteur getAccélération() {
        return accélération;
    }

    public void setAccélération(Vecteur accélération) {
        this.accélération = accélération;
    }

    public int getClef() {
        return clef;
    }

    /**
     * @return the color
     */
    @Override
    public Color getCouleur() {
        return couleur;
    }

    @Override
    protected void setVitesse(Vecteur vitesse) {
        this.vitesse = vitesse;
    }


    public void déplacer(double deltaT)
    {
        Cinematique.mouvementUniformémentAccéléré(this.getPosition(), this.getVitesse(), this.getAccélération(), deltaT);
    }

    public void gestionAccélération(Vector<Bille> billes){
        this.getAccélération().set(Vecteur.VECTEURNUL);
    }

    public void gestionPilotage(AnimationBilles animationBilles) {
    }

    /**
     * gestion de l'éventuelle collision de la bille (this) avec le contour rectangulaire de l'écran défini par (abscisseCoinHautGauche, ordonnéeCoinHautGauche, largeur, hauteur)
     *
     * détecte si il y a collision et le cas échéant met à jour position et vitesse
     *
     * La nature du comportement de la bille en réponse à cette collision est définie dans les classes dérivées
     * */
    public  void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur, double hauteur){

    }



    public void dessine (Graphics g)
    {
        int width, height;
        int xMin, yMin;

        xMin = (int)Math.round(position.x-rayon);
        yMin = (int)Math.round(position.y-rayon);

        width = height = 2*(int)Math.round(rayon);

        g.setColor(couleur);
        g.fillOval( xMin, yMin, width, height);
        g.setColor(Color.CYAN);
        g.drawOval(xMin, yMin, width, height);
    }


    public String toString()
    {
        return "centre = " + position + " rayon = "+rayon +  " vitesse = " + vitesse + " accélération = " + accélération + " couleur = " + couleur + "clef = " + clef;
    }



//----------------- classe Bille -------------------------------------

}
