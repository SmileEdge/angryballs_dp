package angryballs.modele.Comportements.Collisions;

import angryballs.AnimationBilles;
import angryballs.modele.Souris.Souris;
import angryballs.modele.Sound.SoundPlayer;
import mesmaths.cinematique.Collisions;
import angryballs.modele.Bille.Bille;
import angryballs.modele.Bille.DecoratorComportementBille;

import java.util.Vector;

public class Rebond extends DecoratorComportementBille{

    private SoundPlayer sp;
    public Rebond(Bille b, SoundPlayer sp)
    {
        super(b);
        this.sp = sp;
    }

    public void collisionContour(double abscisseCoinHautGauche,
                                 double ordonnéeCoinHautGauche, double largeur, double hauteur)
    {
        //this.bille.collisionContour(abscisseCoinHautGauche, ordonnéeCoinHautGauche, largeur, hauteur);
        if(Collisions.collisionBilleContourAvecRebond( bille.getPosition(), bille.getRayon(), bille.getVitesse(), abscisseCoinHautGauche, ordonnéeCoinHautGauche, largeur, hauteur))
        {
            sp.lancerSon();
            sp.arreterSon();
        }

    }

    @Override
    public void gestionAccélération(Vector<Bille> billes) {
        this.bille.gestionAccélération(billes);
    }

    @Override
    public void gestionPilotage(AnimationBilles animationBilles) {
        this.bille.gestionPilotage(animationBilles);
    }
}
