package angryballs.modele.Comportements.Accelerations;

import angryballs.AnimationBilles;
import angryballs.modele.Souris.Souris;
import mesmaths.cinematique.Collisions;
import mesmaths.mecanique.MecaniquePoint;
import angryballs.modele.Bille.Bille;
import angryballs.modele.Bille.DecoratorComportementBille;

import java.util.Vector;

public class Freinage extends DecoratorComportementBille{
    public Freinage(Bille b)
    {
        super(b);
    }


    public void collisionContour(double abscisseCoinHautGauche,
                                 double ordonnéeCoinHautGauche, double largeur, double hauteur)
    {
        this.bille.collisionContour(abscisseCoinHautGauche, abscisseCoinHautGauche, largeur, hauteur);
        Collisions.collisionBilleContourAvecRebond(bille.getPosition(), bille.getRayon(), bille.getVitesse(), abscisseCoinHautGauche, ordonnéeCoinHautGauche, largeur, hauteur);
    }

    @Override
    public void gestionAccélération(Vector<Bille> billes) {
        this.bille.gestionAccélération(billes);
        this.getAccélération().ajoute(MecaniquePoint.freinageFrottement(bille.masse(), this.getVitesse())); // contribution de l'accélération due au frottement dans l'air
    }

    @Override
    public void gestionPilotage(AnimationBilles animationBilles) {
        this.bille.gestionPilotage(animationBilles);
    }

}
