package angryballs.modele.Comportements.Accelerations;

import angryballs.AnimationBilles;
import angryballs.modele.Souris.Souris;
import mesmaths.geometrie.base.Vecteur;
import angryballs.modele.Bille.Bille;
import angryballs.modele.Bille.DecoratorComportementBille;

import java.util.Vector;

public class AttractionVersBas extends DecoratorComportementBille{

    private Vecteur pesanteur;
    public AttractionVersBas(Bille b, Vecteur pesanteur)
    {
        super(b);
        this.pesanteur = pesanteur;
    }

    @Override
    public void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur, double hauteur) {
        this.bille.collisionContour(abscisseCoinHautGauche, abscisseCoinHautGauche, largeur, hauteur);
    }

    @Override
    public void gestionAccélération(Vector<Bille> billes) {
        this.bille.gestionAccélération(billes);
        this.getAccélération().ajoute(this.pesanteur);          // contribution du champ de pesanteur (par exemple)
    }

    @Override
    public void gestionPilotage(AnimationBilles animationBilles) {
        this.bille.gestionPilotage(animationBilles);
    }
}
