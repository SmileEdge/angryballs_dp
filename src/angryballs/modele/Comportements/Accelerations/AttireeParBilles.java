package angryballs.modele.Comportements.Accelerations;

import angryballs.AnimationBilles;
import angryballs.modele.Bille.Bille;
import angryballs.modele.Bille.DecoratorComportementBille;
import angryballs.modele.Bille.OutilsBille;
import angryballs.modele.Souris.Souris;

import java.util.Vector;

public class AttireeParBilles extends DecoratorComportementBille{

    public AttireeParBilles(Bille b)
    {
        super(b);
    }

    @Override
    public void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur, double hauteur) {
        this.bille.collisionContour(abscisseCoinHautGauche, abscisseCoinHautGauche, largeur, hauteur);
    }

    public void gestionAccélération(Vector<Bille> billes)
    {
        this.bille.gestionAccélération(billes);
        bille.getAccélération().ajoute(OutilsBille.gestionAccélérationNewton(this, billes));     // contribution de l'accélération due é l'attraction des autres billes
    }

    @Override
    public void gestionPilotage(AnimationBilles animationBilles) {
        this.bille.gestionPilotage(animationBilles);
    }
}
